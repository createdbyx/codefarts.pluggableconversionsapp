﻿namespace BlogEnginePagesXmlReader
{
    using System;
    using System.ComponentModel.Composition;
    using System.IO;
    using System.Xml.Linq;
    using Codefarts.ContentManager;
    using Codefarts.PluggableConversionsApp.Attributes;

    public class Page
    {
        /// <summary>
        ///     Gets or sets the unique Identification of the object.
        /// </summary>
        public virtual Guid Id { get; set; }

        /// <summary>
        ///     Gets or sets the parent of the Page. It is used to construct the hierachy of the pages.
        /// </summary>
        public Guid Parent { get; set; }

        /// <summary>
        ///     Gets or sets the Title or the object.
        /// </summary>
        public string Title { get; set; }

        /// <summary>
        ///     Gets or sets the Title or the object.
        /// </summary>
        public string[] Keywords { get; set; }

        /// <summary>
        ///     Gets or sets the date on which the instance was created.
        /// </summary>
        public DateTime DateCreated { get; set; }

        /// <summary>
        ///     Gets or sets the date on which the instance was modified.
        /// </summary>
        public DateTime DateModified { get; set; }

        /// <summary>
        ///     Gets or sets the Description or the object.
        /// </summary>
        public string Content { get; set; }
    }

    [ReaderPlugin("Blog Engine Page Reader")]
    [Export(typeof(IReader<string>))]
    public class PageReader : IReader<string>
    {
        public Type Type
        {
            get
            {
                return typeof(Page);
            }
        }

        public object Read(string key, ContentManager<string> content)
        {
            var path = Path.Combine(content.RootDirectory, key);

            using (var stream = File.OpenRead(path))
            {
                var doc = XDocument.Load(stream);
                var page = new Page();
                page.Id = Guid.Parse(Path.GetFileNameWithoutExtension(path));
                page.Title = doc.Root.Element("title").Value;
                page.Content = doc.Root.Element("content").Value;
                page.Parent = Guid.Parse(doc.Root.Element("parent").Value);
                page.DateCreated = DateTime.Parse(doc.Root.Element("datecreated").Value);
                page.DateModified = DateTime.Parse(doc.Root.Element("datemodified").Value);

                return page;
            }
        }

        public bool CanRead(string key, ContentManager<string> content)
        {
            var path = Path.Combine(content.RootDirectory, key);
            return File.Exists(path);
        }

        public void ReadAsync(string key, ContentManager<string> content, Action<ReadAsyncArgs<string, object>> completedCallback)
        {
            throw new NotImplementedException();
        }
    }
}
