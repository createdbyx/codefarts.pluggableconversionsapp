﻿namespace Codefarts.PluggableConversionsApp.Models
{
    using System.Collections.Generic;
    using System.Collections.ObjectModel;

    public class WritersCollection : ObservableCollection<PluginInformation>
    {
        public WritersCollection()
        {
        }

        public WritersCollection(List<PluginInformation> list)
            : base(list)
        {
        }

        public WritersCollection(IEnumerable<PluginInformation> collection)
            : base(collection)
        {
        }
    }
}