﻿namespace Codefarts.PluggableConversionsApp.Models
{
    using System.Collections.Generic;
    using System.Collections.ObjectModel;

    public class ReadersCollection : ObservableCollection<PluginInformation>
    {
        public ReadersCollection()
        {
        }

        public ReadersCollection(List<PluginInformation> list)
            : base(list)
        {
        }

        public ReadersCollection(IEnumerable<PluginInformation> collection)
            : base(collection)
        {
        }
    }
}