﻿using System.Collections.ObjectModel;
using Codefarts.AppCore;
using Codefarts.PluggableConversionsApp.Interfaces;

namespace Codefarts.PluggableConversionsApp.Models
{
    public class PluginsModel : PropertyChangedBase
    {
        private ReadersCollection readerPlugins;
        private ApplicationModel application;
        private WritersCollection writerPlugins;
        //private ObservableCollection<PluginInformation> resultPlugins;
        private ObservableCollection<PluginInformation> generalPluginInformation;
        private ObservableCollection<IPlugin> generalPlugins;

        public ObservableCollection<IPlugin> GeneralPlugins
        {
            get
            {
                return this.generalPlugins;
            }

            set
            {
                var pValue = this.generalPlugins;
                if (pValue != value)
                {
                    this.generalPlugins = value;
                    this.NotifyOfPropertyChange(() => this.GeneralPlugins);
                }
            }
        }

        public ObservableCollection<PluginInformation> GeneralPluginInformation
        {
            get
            {
                return this.generalPluginInformation;
            }

            set
            {
                var pValue = this.generalPluginInformation;
                if (pValue != value)
                {
                    this.generalPluginInformation = value;
                    this.NotifyOfPropertyChange(() => this.GeneralPluginInformation);
                }
            }
        }

        //public ObservableCollection<PluginInformation> ResultPlugins
        //{
        //    get
        //    {
        //        return this.resultPlugins;
        //    }

        //    set
        //    {
        //        var pValue = this.resultPlugins;
        //        if (pValue != value)
        //        {
        //            this.resultPlugins = value;
        //            this.NotifyOfPropertyChange(() => this.ResultPlugins);
        //        }
        //    }
        //}

        public WritersCollection WriterPlugins
        {
            get
            {
                return this.writerPlugins;
            }

            set
            {
                var pValue = this.writerPlugins;
                if (pValue != value)
                {
                    this.writerPlugins = value;
                    this.NotifyOfPropertyChange(() => this.WriterPlugins);
                }
            }
        }

        public PluginsModel(ApplicationModel application)
        {
            this.application = application;
        }

        public ReadersCollection ReaderPlugins
        {
            get
            {
                return this.readerPlugins;
            }

            set
            {
                var pValue = this.readerPlugins;
                if (pValue != value)
                {
                    this.readerPlugins = value;
                    this.NotifyOfPropertyChange(() => this.ReaderPlugins);
                }
            }
        }
    }
}