﻿using Codefarts.AppCore;

namespace Codefarts.PluggableConversionsApp.Models
{
    public class ApplicationModel : PropertyChangedBase
    {
        private LoggingDataModel logging;
          private PluginsModel plugins;

        public PluginsModel Plugins
        {
            get
            {
                return this.plugins;
            }

            internal set
            {
                var currentValue = this.plugins;
                if (currentValue != value)
                {
                    this.plugins = value;
                    this.NotifyOfPropertyChange(() => this.Plugins);
                }
            }
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="T:System.Object"/> class.
        /// </summary>
        public ApplicationModel()
        {
            this.Plugins = new PluginsModel(this);
            this.logging = new LoggingDataModel(this);
        }
    }
}