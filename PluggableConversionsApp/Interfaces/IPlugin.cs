﻿namespace Codefarts.PluggableConversionsApp.Interfaces
{
    using Codefarts.PluggableConversionsApp.Models;

    /// <summary>
    /// Provides a interface for source plugins.
    /// </summary>
    public interface IPlugin
    {
        /// <summary>
        /// Gets the user friendly title.
        /// </summary>
        /// <remarks>This provides a user friendly descriptor for the implementor that can be used in ui to identify the object.</remarks>
        string Title { get; }

        void Connect(ApplicationModel appModel);

        void Disconnect();
    }
}