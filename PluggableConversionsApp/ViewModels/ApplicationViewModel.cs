﻿using System.Windows.Input;
using Codefarts.WPFCommon.Commands;

namespace Codefarts.PluggableConversionsApp.ViewModels
{
    using Codefarts.AppCore;
    using Codefarts.PluggableConversionsApp.Models;

    public class ApplicationViewModel : PropertyChangedBase
    {
        private ApplicationModel applicationModel;

        private string selectedSourceFile;

        public string SelectedSourceFile
        {
            get { return this.selectedSourceFile; }

            set
            {
                var currentValue = this.selectedSourceFile;
                if (currentValue != value)
                {
                    this.selectedSourceFile = value;
                    this.NotifyOfPropertyChange(() => this.SelectedSourceFile);
                }
            }
        }

        public ApplicationModel Application
        {
            get { return this.applicationModel; }

            set
            {
                var currentValue = this.applicationModel;
                if (currentValue != value)
                {
                    this.applicationModel = value;
                    this.NotifyOfPropertyChange(() => this.Application);
                }
            }
        }

        public ICommand ShowSourceDialog
        {
            get
            {

                return new OpenFileCommand(file => this.SelectedSourceFile = file, "Any File|*.*", true);
            }
        }

        public ApplicationViewModel(ApplicationModel applicationModel)
        {
            this.applicationModel = applicationModel;
        }
    }
}
