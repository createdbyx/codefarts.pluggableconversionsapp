﻿namespace Codefarts.PluggableConversionsApp.Attributes
{
    using System;

    [AttributeUsage(AttributeTargets.Class)]
    public class ReaderPluginAttribute : GeneralPluginAttribute
    {
        public ReaderPluginAttribute(string title) : base(title)
        {
        }
    }
}