﻿namespace Codefarts.PluggableConversionsApp.Attributes
{
    using System;

    public abstract class PluginHandlesTypeAttribute : GeneralPluginAttribute
    {
        public Type[] HandlesTypes { get; private set; }

        protected PluginHandlesTypeAttribute(string title, Type[] handlesTypes)
            : base(title)
        {
            this.HandlesTypes = handlesTypes;
        }
    }
}