﻿namespace Codefarts.PluggableConversionsApp
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.ComponentModel;
    using System.ComponentModel.Composition;
    using System.ComponentModel.Composition.Primitives;
    using System.ComponentModel.Composition.ReflectionModel;
    using System.IO;
    using System.Linq;
    using System.Windows;
    using Codefarts.AppCore;
    using Codefarts.ContentManager;
    using Codefarts.PluggableConversionsApp.Attributes;
    using Codefarts.PluggableConversionsApp.Interfaces;
    using Codefarts.PluggableConversionsApp.Models;
    using Codefarts.PluggableConversionsApp.ViewModels;
    using Codefarts.ViewMessaging;

    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        protected override void OnStartup(StartupEventArgs e)
        {
            base.OnStartup(e);

            PlatformProvider.Current = new WpfPlatformProvider();
            var viewService = new WpfViewService();
            this.Properties["ViewService"] = viewService;

            var applicationModel = new ApplicationModel();
            var applicationViewModel = new ApplicationViewModel(applicationModel);
            var parts = this.LoadPlugins(applicationModel);

            var applicationPlugins = applicationViewModel.Application.Plugins;
            applicationPlugins.GeneralPlugins = new ObservableCollection<IPlugin>(parts.GeneralPlugins);
            applicationPlugins.GeneralPluginInformation = new ObservableCollection<PluginInformation>(parts.GeneralPluginInformation);
            applicationPlugins.ReaderPlugins = new ReadersCollection(parts.ReaderPlugins);
            applicationPlugins.WriterPlugins = new WritersCollection(parts.WriterPlugins);

            // show main window
            var mainView = viewService.CreateView("Application");
            var window = mainView.ViewReference as Window;
            this.MainWindow = window;
            var args = GenericMessageArguments.Build(
                GenericMessageArguments.Show,
                GenericMessageArguments.SetModel(applicationViewModel));
            mainView.SendMessage(args);

            // connect source plugins
            this.ConnectPlugins(applicationViewModel, applicationPlugins.GeneralPlugins);
            // applicationModel.Logging.Logs.Add("Status");
        }

        private void ConnectPlugins(ApplicationViewModel applicationViewModel, IEnumerable<IPlugin> plugins)
        {
            foreach (var item in plugins)
            {
                item.Connect(applicationViewModel.Application);
            }
        }

        internal class MEFComponents
        {
            [ImportMany(typeof(ResourceDictionary))]
            public IEnumerable<ResourceDictionary> ResourceDictionaries { get; set; }

            // [ImportMany(typeof(ISourcePlugin))]
            public IEnumerable<PluginInformation> ReaderPlugins { get; set; }

            // [ImportMany(typeof(ISearchFilter))]
            public IEnumerable<PluginInformation> WriterPlugins { get; set; }

            //// [ImportMany(typeof(IResultsPlugin))]
            //public IEnumerable<PluginInformation> ResultPlugins { get; set; }

            // [ImportMany(typeof(IPlugin))]
            public IEnumerable<PluginInformation> GeneralPluginInformation { get; set; }

            [ImportMany(typeof(IPlugin))]
            public IEnumerable<IPlugin> GeneralPlugins { get; set; }
        }

        private MEFComponents LoadPlugins(ApplicationModel appModel)
        {
            var parts = new MEFComponents();
            var pluginsPath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Plugins");
            Directory.CreateDirectory(pluginsPath);
            var searchPaths = Directory.GetDirectories(pluginsPath, "*.*", SearchOption.TopDirectoryOnly);
            var composer = MEFHelpers.Compose(searchPaths, parts);

            parts.ReaderPlugins = this.GetPluginInformation<IReader<string>>(composer.Catalog, appModel);
            parts.WriterPlugins = this.GetPluginInformation<IWriter<string>>(composer.Catalog, appModel);
            parts.GeneralPluginInformation = this.GetGeneralPluginInformation(composer.Catalog, appModel);
            // parts.ResultPlugins = this.GetPluginInformation<IResultsPlugin>(composer.Catalog, appModel);
            return parts;
        }

        private IEnumerable<PluginInformation> GetPluginInformation<TPluginType>(ComposablePartCatalog composerCatalog,
            ApplicationModel appModel)
        {
            var exportedTypes = this.GetExportedTypes<TPluginType>(composerCatalog);
            var results = exportedTypes.Select(x =>
            {
                var attribute = x.GetCustomAttributes(true).OfType<ReaderPluginAttribute>().FirstOrDefault();
                var categoryAttribute = x.GetCustomAttributes(true).OfType<CategoryAttribute>().FirstOrDefault();
                var descAttribute = x.GetCustomAttributes(true).OfType<DescriptionAttribute>().FirstOrDefault();

                if (attribute == null)
                {
                    return null;
                }

                var description = descAttribute == null ? string.Empty : descAttribute.Description;
                var category = categoryAttribute == null ? string.Empty : categoryAttribute.Category;
                //  appModel.Logging.Logs.Add(string.Format("Plugin type '{0}' found: \"{1}\"", typeof(TPluginType).Name, attribute.Title));
                return new PluginInformation(attribute.Title, category, x, description);
            }).Where(x => x != null);

            return results.ToArray();
        }

        private IEnumerable<PluginInformation> GetGeneralPluginInformation(ComposablePartCatalog composerCatalog,
            ApplicationModel appModel)
        {
            var exportedTypes = this.GetExportedTypes<IPlugin>(composerCatalog);
            var results = exportedTypes.Select(x =>
            {
                var attribute = x.GetCustomAttributes(true).OfType<GeneralPluginAttribute>().FirstOrDefault();
                var categoryAttribute = x.GetCustomAttributes(true).OfType<CategoryAttribute>().FirstOrDefault();
                var descAttribute = x.GetCustomAttributes(true).OfType<DescriptionAttribute>().FirstOrDefault();

                if (attribute == null)
                {
                    return null;
                }

                var description = descAttribute == null ? string.Empty : descAttribute.Description;
                var category = categoryAttribute == null ? string.Empty : categoryAttribute.Category;
                // appModel.Logging.Logs.Add(string.Format("General plugin found: \"{0}\"", attribute.Title));
                return new PluginInformation(attribute.Title, category, x, description);
            }).Where(x => x != null);

            return results.ToArray();
        }

        public IEnumerable<Type> GetExportedTypes<T>(ComposablePartCatalog catalog)
        {
            return catalog.Parts.Select(part => this.ComposablePartExportType<T>(part)).Where(t => t != null).ToArray();
        }

        private Type ComposablePartExportType<T>(ComposablePartDefinition part)
        {
            if (part.ExportDefinitions.Any(
                def => def.Metadata.ContainsKey("ExportTypeIdentity") &&
                       def.Metadata["ExportTypeIdentity"].Equals(GetIdentityName<T>())))
            {
                return ReflectionModelServices.GetPartType(part).Value;
            }

            return null;
        }

        private static string GetIdentityName<T>()
        {
            var type = typeof(T);
            var typeName = type.Name.Contains("`") ? type.Name.Substring(0, type.Name.IndexOf("`")) : type.Name;
            var name = type.Namespace + "." + typeName;
            var parts = string.Join(", ", type.GetGenericArguments().Select(x => x.FullName /* x.Namespace + "." + x.Name*/));
            name += string.IsNullOrWhiteSpace(parts) ? string.Empty : "(" + parts + ")";
            return name;
        }
    }
}
